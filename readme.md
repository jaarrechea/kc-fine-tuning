# KC-FINE-TUNING

El objetivo de este modelo es crear un modelo que detecte de forma conjunta los tipos de estancias
de una vivienda, concretamente, baños, dormitorios, cocinas y salas de estar.

El programa principal de este repositorio se encuentra en _train.py_, que es el encargado de crear el 
modelo aplicando fine tuning a una red VGG16. Las imágenes que utiliza se encuentran bajo el 
directorio dataset, el cual ha sido preparado por el programa _build_dataset.py_.
 
Este proyecto está relacionado con el proyecto kc-tfb, https://gitlab.com/jaarrechea/kc-tfb, que es el Trabajo Fin de Bootcamp de Full Stack Big Data, AI & ML. 