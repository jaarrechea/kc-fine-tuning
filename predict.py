# Uso
# python predict.py --image prediction/BE00001.jpeg

# paquetes necesarios
from tensorflow.keras.models import load_model
import config
import numpy as np
import argparse
import imutils
import cv2
import os

# argumentos: imagen que se quiere evaluar
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", type=str, required=True,
	help="path to our input image")
args = vars(ap.parse_args())

# Cargamos la imagen que queremos evaluar y en la que escribiremos el resultado
image_path = args["image"]
image_name = image_path.split(os.sep)[-1]
image = cv2.imread(image_path)
output = image.copy()
output = imutils.resize(output, width=400)

# OpenCV trata las imágenes en formato BGR y por tanto tenemos que pasarlo a RGB.
# También redimensionamos la imgaen a 224x224 porque son las dimensiones de VGG16
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
image = cv2.resize(image, (224, 224))

# Con el objeto de normalizar/escalar la imagen, creamos un array con la media
# de los valores de los colores y se lo restamos a la imagen
image = image.astype("float32")
mean = np.array([123.68, 116.779, 103.939][::1], dtype="float32")
image -= mean

# Cargamos el modelo obenido en train
print("[INFO] loading model...")
model = load_model(config.MODEL_PATH)

# Analizamos la imagen y realizamos la predicción sobre qué tipo de imagen es
preds = model.predict(np.expand_dims(image, axis=0))[0]
i = np.argmax(preds)
label = config.CLASSES[i]

print(f'Prediction: "{label}: {preds[i] * 100:.2f}%"')
# Escribimos la predicción en la imagen
text = "{}: {:.2f}%".format(label, preds[i] * 100)
cv2.putText(output, text, (3, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
	(0, 255, 0), 2)

# Mostramos la imagen
output_image_path = os.path.sep.join([config.OUTPUT_PREDICTION_IMAGES_PATH, image_name])
cv2.imwrite(output_image_path, output)
cv2.imshow("Output", output)
cv2.waitKey(0)