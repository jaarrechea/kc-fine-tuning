import os

import cv2
from tensorflow.keras.applications.resnet50 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.models import load_model

import config
from imutils import paths
import numpy as np

# Images que se van a procesar para obtener las mejores imágenes de baño, dormitorio, cocina y salón
image_paths = list(paths.list_images(config.PREDICTION_PATH))
images = []
# Media de colores para realizar normalización de las imágenes
mean = np.array([123.68, 116.779, 103.939][::1], dtype="float32")
for image_path in image_paths:

    # Leemos la imagen
    image = cv2.imread(image_path)

    # OpenCV trata las imágenes en formato BGR y por tanto tenemos que pasarlo a RGB.
    # También redimensionamos la imgaen a 224x224 porque son las dimensiones de VGG16
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224, 224))

    # Con el objeto de normalizar/escalar la imagen, creamos un array con la media
    # de los valores de los colores y se lo restamos a la imagen
    image = image.astype("float32")
    image -= mean

    # add the image to the batch
    image = np.expand_dims(image, axis=0)
    images.append(image)

model = load_model(config.MODEL_PATH)
images = np.vstack(images)
preds = model.predict(images, batch_size=config.BATCH_SIZE)

# Determinar los nombres de las mejroes fotos de cada clase
best_photos = []
num_photo = 0
for num_photo, room in enumerate(config.CLASSES):
    best_photo = np.argmax(preds[:, num_photo])
    image_path = image_paths[best_photo]
    print(f'La mejor foto {room} es {image_path}')
    best_photos.append(image_path)

# Construimos un mosacio con las 4 mejores fotos con un tamaño de 224 * 224
# Inicializamos una lista de imágenes de entragada, así como la variable de la imagen mosaico de salida
outputImage = np.zeros((224, 224, 3), dtype="uint8")
inputImages = []
# loop over the best photos
for photo in best_photos:
    # load the input image, resize it to be 32 32, and then
    # update the list of input images
    image = cv2.imread(photo)
    image = cv2.resize(image, (112, 112))
    inputImages.append(image)
# tile the four input images in the output image such the first
# image goes in the top-right corner, the second image in the
# top-left corner, the third image in the bottom-left corner,
# and the final image in the bottom-right corner
outputImage[0:112, 0:112] = inputImages[0]
outputImage[0:112, 112:224] = inputImages[1]
outputImage[112:224, 0:112] = inputImages[2]
outputImage[112:224, 112:224] = inputImages[3]
# add the tiled image to our set of images the network will be
# trained on
tile = os.path.sep.join([config.OUTPUT_TILE_IMAGE_PATH, "tile.jpg"])
cv2.imwrite(tile, outputImage)

