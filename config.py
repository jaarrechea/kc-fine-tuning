# import the necessary packages
import os

# initialize the path to the *original* input directory of images
ORIG_INPUT_DATASET = "rooms"

# initialize the base path to the *new* directory that will contain
# our images after computing the training and testing split
BASE_PATH = "dataset"

# define the names of the training, testing, and validation
# directories
TRAIN = "training"
TEST = "test"
VAL = "validation"

# initialize the list of class label names
CLASSES = ["bathroom", "bedroom", "kitchen", "livingroom"]

# set the batch size when fine-tuning
BATCH_SIZE = 32

# initialize the label encoder file path and the output directory to
# where the extracted features (in CSV file format) will be stored
LE_PATH = os.path.sep.join(["output", "le.cpickle"])
BASE_CSV_PATH = "output"

# set the path to the serialized model after training
MODEL_PATH = os.path.sep.join(["output", "rooms.model"])
PRE_MODEL_PATH = os.path.sep.join(["output", "rooms.premodel"])

# define the path to the output training history plots
UNFROZEN_PLOT_PATH = os.path.sep.join(["output", "unfrozen.png"])
WARMUP_PLOT_PATH = os.path.sep.join(["output", "warmup.png"])

PREDICTION_PATH = "predictions"
OUTPUT_PREDICTION_IMAGES_PATH = "output"
OUTPUT_TILE_IMAGE_PATH = "output"